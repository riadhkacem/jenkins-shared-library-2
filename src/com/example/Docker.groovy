#!/usr/bin/env groovy
package com.example

class Docker implements Serializable {
    def script

    Docker(script) {
        this.script = script
    }

    def buildImage(String imageName) {
        script.echo 'building docker image...'
        script.sh "docker build -t ${imageName} ."
    }
    def dockerLogin() {
        script.withCredentials([script.usernamePassword([credentialsId : 'dockerhub-credentials', usernameVariable : 'USER', passwordVariable : 'PASS'])]) {
            script.sh 'echo ${PASS} | docker login -u ${USER} --password-stdin'
        }
    }
    def dockerPush(String imageName) {
        script.echo 'push image to dockerhub...'
        script.sh "docker push ${imageName}"
    }
}
