#!/usr/bin/env groovy
import com.example.Docker

def call() {
    new Docker(this).dockerLogin()
}